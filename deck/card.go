//go:generate stringer -type=Suit
//go:generate stringer -type=Rank

package deck

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

type Suit uint8

const (
	Spade Suit = iota
	Diamond
	Club
	Heart
	Joker // this is a special case
)

var suits = [...]Suit{Spade, Diamond, Club, Heart}

type Rank uint8

const (
	Ace Rank = iota + 1
	Two
	Three
	Four
	Five
	Six
	Seven
	Eight
	Nine
	Ten
	Jack
	Queen
	King
)

const (
	minRank = Ace
	maxRank = King
)

// Card in the deck of cards, or a house
type Card struct {
	Suit
	Rank
}

func (c Card) String() string {
	if c.Suit == Joker {
		return c.Suit.String()
	}
	return fmt.Sprintf("%s of %ss", c.Rank.String(), c.Suit.String())
}

// New creates a new sorted deck of cards
func New(opts ...func([]Card) []Card) []Card {
	var cards []Card
	for _, s := range suits {
		for r := minRank; r <= maxRank; r++ {
			cards = append(cards, Card{Suit: s, Rank: r})
		}
	}
	for _, opt := range opts {
		cards = opt(cards)
	}
	return cards
}

// DefaultSort min to max.
func DefaultSort(cards []Card) []Card {
	sort.Slice(cards, Less(cards))
	return cards
}

// Sort a slice of cards base on the rules of `less` function.
func Sort(less func(cards []Card) func(i, j int) bool) func([]Card) []Card {
	return func(cards []Card) []Card {
		sort.Slice(cards, less(cards))
		return cards
	}
}

// Less returns a function that compares cards in a deck.
func Less(cards []Card) func(i, j int) bool {
	return func(i, j int) bool {
		return absRank(cards[i]) < absRank(cards[j])
	}
}

func absRank(c Card) int {
	return int(c.Suit)*int(maxRank) + int(c.Rank)
}

type Permer interface {
	Perm(n int) []int
}

var shuffleRand = rand.New(rand.NewSource(time.Now().Unix()))

// Shuffle a slice of cards.
func Shuffle(cards []Card) []Card {
	ret := make([]Card, len(cards))
	for i, j := range shuffleRand.Perm(len(cards)) {
		ret[i] = cards[j]
	}
	return ret
}

// Jokers takes the number of jokers that you want in your deck
func Jokers(n int) func([]Card) []Card {
	return func(cards []Card) []Card {
		for i := 0; i < n; i++ {
			cards = append(cards, Card{Suit: Joker, Rank: Rank(i)})
		}
		return cards
	}
}

func Filter(f func(card Card) bool) func([]Card) []Card {
	return func(cards []Card) []Card {
		var result []Card
		for _, c := range cards {
			if !f(c) {
				result = append(result, c)
			}
		}
		return result
	}
}

// Deck generate duplicate decks.
func Deck(n int) func([]Card) []Card {
	return func(cards []Card) []Card {
		var result []Card
		for i := 0; i < n; i++ {
			result = append(result, cards...)
		}
		return result
	}
}
