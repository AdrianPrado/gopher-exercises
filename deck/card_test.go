//go:generate stringer -type=Suit
//go:generate stringer -type=Rank

package deck

import (
	"math/rand"
	"reflect"
	"testing"
)

func TestCard_String(t *testing.T) {
	tests := []struct {
		name string
		card Card
		want string
	}{
		{
			name: "Ace of Hearts",
			card: Card{Rank: Ace, Suit: Heart},
			want: "Ace of Hearts",
		},
		{
			name: "King of Hearts",
			card: Card{Rank: King, Suit: Diamond},
			want: "King of Diamonds",
		},
		{
			name: "Two of Clubs",
			card: Card{Rank: Two, Suit: Club},
			want: "Two of Clubs",
		},
		{
			name: "Ten of Spades",
			card: Card{Rank: Ten, Suit: Spade},
			want: "Ten of Spades",
		},
		{
			name: "Joker",
			card: Card{Suit: Joker},
			want: "Joker",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := Card{
				Suit: tt.card.Suit,
				Rank: tt.card.Rank,
			}
			if got := c.String(); got != tt.want {
				t.Errorf("Card.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNew(t *testing.T) {
	tests := []struct {
		name string
		want int
	}{
		{
			name: "test deck size",
			want: 52,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := len(New()); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDefaultSort(t *testing.T) {
	tests := []struct {
		name     string
		deckSize int
		position int
		card     Card
	}{
		{
			name:     "Check Ace of Spades",
			deckSize: 52,
			position: 0,
			card:     Card{Rank: Ace, Suit: Spade},
		},
		{
			name:     "Check King of Spades",
			deckSize: 52,
			position: 12,
			card:     Card{Rank: King, Suit: Spade},
		},
		{
			name:     "Check King of Hearts",
			deckSize: 52,
			position: 51,
			card:     Card{Rank: King, Suit: Heart},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cards := New(DefaultSort)
			if got := len(cards); !reflect.DeepEqual(got, tt.deckSize) {
				t.Errorf("New() = %v, want %v", got, tt.deckSize)
			}
			if card := cards[tt.position]; !reflect.DeepEqual(card, tt.card) {
				t.Errorf("Card %v in position %v, got %v", card, tt.position, tt.card)
			}
		})
	}
}

func TestSort(t *testing.T) {
	tests := []struct {
		name     string
		deckSize int
		position int
		card     Card
	}{
		{
			name:     "Check Ace of Spades",
			deckSize: 52,
			position: 0,
			card:     Card{Rank: Ace, Suit: Spade},
		},
		{
			name:     "Check King of Spades",
			deckSize: 52,
			position: 12,
			card:     Card{Rank: King, Suit: Spade},
		},
		{
			name:     "Check King of Hearts",
			deckSize: 52,
			position: 51,
			card:     Card{Rank: King, Suit: Heart},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cards := New(Sort(Less))
			if got := len(cards); !reflect.DeepEqual(got, tt.deckSize) {
				t.Errorf("New() = %v, want %v", got, tt.deckSize)
			}
			if card := cards[tt.position]; !reflect.DeepEqual(card, tt.card) {
				t.Errorf("Card %v in position %v, got %v", card, tt.position, tt.card)
			}
		})
	}
}

func TestJokers(t *testing.T) {
	tests := []struct {
		name   string
		jokers int
		length int
	}{
		{
			name:   "Test with no jokers",
			jokers: 0,
			length: 52,
		},
		{
			name:   "Test with one jokers",
			jokers: 1,
			length: 53,
		},
		{
			name:   "Test with five jokers",
			jokers: 5,
			length: 57,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cards := New(Jokers(tt.jokers))
			if got := len(cards); !reflect.DeepEqual(got, tt.length) {
				t.Errorf("New() = %v, want %v", got, tt.length)
			}
			jokers := 0
			for _, c := range cards {
				if c.Suit == Joker {
					jokers++
				}
			}
			if jokers != tt.jokers {
				t.Errorf("Jokers in deck = %v, want %v", jokers, tt.jokers)
			}
		})
	}
}

func TestFilter(t *testing.T) {
	type filter struct {
		f func(card Card) bool
	}
	tests := []struct {
		name   string
		length int
		filter filter
	}{
		{
			name:   "filter Two's and Tree's",
			length: 44,
			filter: filter{
				f: func(c Card) bool {
					return c.Rank == Two || c.Rank == Three
				},
			},
		},
		{
			name:   "filter Spades",
			length: 39,
			filter: filter{
				f: func(c Card) bool {
					return c.Suit == Spade
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cards := New(Filter(tt.filter.f))
			if got := len(cards); !reflect.DeepEqual(got, tt.length) {
				t.Errorf("Filter() = %v, want %v", got, tt.length)
			}
			for _, c := range cards {
				if tt.filter.f(c) {
					t.Errorf("Filter() = %v, should have been filtered", c)
				}
			}
		})
	}
}

func TestDeck(t *testing.T) {
	tests := []struct {
		name     string
		numDecks int
		length   int
	}{
		{
			name:     "double deck",
			numDecks: 2,
			length:   52 * 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := len(New(Deck(tt.numDecks))); !reflect.DeepEqual(got, tt.length) {
				t.Errorf("Deck() = %v, want %v", got, tt.length)
			}
		})
	}
}

func TestShuffle(t *testing.T) {
	// shuffleRand deterministic
	// First call to shuffleRand will return
	// [40, 35...]
	shuffleRand = rand.New(rand.NewSource(0))
	tests := []struct {
		name     string
		shuffled []int
	}{
		{
			name:     "Test deterministic 0",
			shuffled: []int{40, 35},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cards := New()
			shuffled := New(Shuffle)
			for i, j := range tt.shuffled {
				if cards[j] != shuffled[i] {
					t.Errorf("Shuffle() = %v, want %v", shuffled[i], cards[j])
				}
			}
		})
	}
}
