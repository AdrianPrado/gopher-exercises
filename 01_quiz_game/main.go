package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// Problem in csv file
type Problem struct {
	number0 string
	number1 string
	result  string
}

// Check if numbers addition equals result
func (t *Problem) Check() bool {
	s0, _ := strconv.Atoi(t.number0)
	s1, _ := strconv.Atoi(t.number1)
	re, _ := strconv.Atoi(t.result)
	foo, _ := strconv.
	if s0+s1 == re {
		return true
	}
	return false
}

func main() {
	fileName := flag.String("csv", "foo", "csv file name with test.")
	timeLimit := flag.Int("time", 30, "csv file name with test.")
	flag.Parse()

	fmt.Println("filename ", *fileName)

	tests, err := readProblems(*fileName)
	if err != nil {
		panic(err)
	}

	timer := time.NewTimer(time.Duration(*timeLimit) * time.Second)
	correct := 0

problemsLoop:
	for i, t := range tests {
		fmt.Printf("Problem #%d, %v", i+1, t)
		answerCh := make(chan string)
		go func() {
			var answer string
			fmt.Scanf("%s\n", &answer)
			answerCh <- answer
		}()

		select {
		case <-timer.C:
			fmt.Println()
			break problemsLoop
		case answer := <-answerCh:
			if answerCh == t.result {
				correct++
			}
		}
	}
	fmt.Printf("You scored %d out of %d.\n", correct, len(problems))
}

func readProblems(fileName string) ([]Problem, error) {
	f, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	// Read file into variable
	lines, err := csv.NewReader(f).ReadAll()
	if err != nil {
		return nil, err
	}
	regexpString := `(\d)`
	r, err := regexp.Compile(regexpString)
	if err != nil {
		return nil, err
	}
	result := make([]Problem, len(lines))
	for i, line := range lines {
		s := r.FindAllString(line[0], 2)
		t := Problem{
			number0: s[0],
			number1: s[1],
			result:  strings.TrimSpace(line[1]),
		}
		result[i] = t
	}
	return result, nil
}
